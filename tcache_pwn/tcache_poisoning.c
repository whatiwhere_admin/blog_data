#include <malloc.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


int main(int argc, const char* argv[]) {
    size_t target[6];
    printf("stack: %p\n",target);
    char *mem = malloc(0x48);
    free(mem);
    *(long *)(mem) = (long)target; 
    char *mem1 = malloc(0x48);
    char *mem2 = malloc(0x48);
    printf("mem2: %p\n", mem2);
    return 0;
}

