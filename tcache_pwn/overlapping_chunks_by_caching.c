#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
int main(int argc, const char* argv[]) {
    char *mem = malloc(0x48);
    char *sentry = malloc(0x18);
    printf("mem: %p, sentry: %p\n",mem, sentry);
    *(long* )(mem - sizeof(long)) = 0x110;  // 设置 chunk->size = 0x110
    free(mem);
    char *mem2 = malloc(0x100);  // 分配一个 0x110 的chunk
    printf("mem2: %p\n", mem2);
    return 0;
}

